---
title: Star athlete or engineer? The Big Five in tech are paying like the Big Four in sports
description: The big four sports franchises and the FAANG are now paying in the same ball park.
layout: post
date: '2019-11-30 23:30:00'
tags: JavaScript data-structures algorithms
subclass: post tag-test tag-content
categories: computer-science
navigation: 'True'
image: 'ds-algo.png'
article: 'True'
---

Inspired by https://twitter.com/GatorsScott/status/640865339320999936

Total people in CS 1,256,200 (https://www.bls.gov/ooh/computer-and-information-technology/software-developers.htm)

College CS majors 55,000 (https://danwang.co/why-so-few-computer-science-majors/)

% CS Grads who stick it out 64% (https://datausa.io/profile/cip/110701/)

CS grads 35,329 (https://datausa.io/profile/cip/110701/)

% of CS grads to FAANG 0.2% (https://qz.com/285001/heres-why-you-only-have-a-0-2-chance-of-getting-hired-at-google/)

People who make it into FAANG 12,500 (https://qz.com/285001/heres-why-you-only-have-a-0-2-chance-of-getting-hired-at-google/)

% of FAANG who make it to Staff+ 15% (https://www.quora.com/How-does-Googles-internal-levels-work)

People who make it to Staff engineer at Google 1,875 (https://www.quora.com/How-does-Googles-internal-levels-work)

Average income $506,000 (https://www.levels.fyi/SE/Google/Facebook/Amazon)

Income after taxes $292,400

Compared to professional starting salaries (https://cashmoneylife.com/how-much-does-a-professional-benchwarmer-earn/)

* NFL = $465,000
* MLB = $507,500
* NBA = $582,180
* NHL = $650,000

A Staff engineer at Google or Facebook stands to make more than the rookies in the NFL and MLB. In fact many more non-rookies spend years (particularly in the MLB, NHL, and MLB) playing in minor leagues making far less, or play part time (plenty of NBA players make less than $100k a year).
