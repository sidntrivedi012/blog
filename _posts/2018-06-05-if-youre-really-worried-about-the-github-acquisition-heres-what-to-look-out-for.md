---
title: If you're really worried about the GitHub acquisition, here's what to look out for
description: GitHub was purchased by Microsoft on June 4th. Let's understand why and what you can do about it if you no longer believe GitHub is worth storing your code on.
layout: post
date: '2018-06-04 21:13:00'
tags: github microsoft terms-of-service legal
subclass: post tag-test tag-content
categories: news
navigation: 'True'
image: 'msft-better-w-open-source.png'
article: 'True'
---

Today GitHub was [officially acquired by Microsoft](https://blog.github.com/2018-06-04-github-microsoft/). Many of the internet's citizens are [noticeably freaked out](https://github.com/upend/IF_MS_BUYS_GITHUB_IMMA_OUT) about this. I believe this is unnecessary and will attempt to explain why in this essay.

I believe that developers' concerns boil down to two issues: that Microsoft is evil and will corrupt GitHub, and that the alternatives (primarily GitLab and Bitbucket) are independent, and therefore less/not evil. Once I show that neither issue is true, I'll explain what to look out for when the acquisition is complete and the terms of service are updated to reflect the policies that Microsoft would like to enact.

Full disclosure: I used to work for Microsoft. This was nearly a decade ago, and I have no idea what goes on behind their walls. All of my connections who worked there now work somewhere else. I figure I should say this now because someone will try to be an internet sleuth and attempt to invalidate my arguments because of that connection. I don't think it plays a role, but you'll have to judge for yourself.

# Myth: Microsoft is evil and will corrupt GitHub

This is the primary reaction I see from the development community. Developers have long hated Microsoft, and this mostly stems from the fact that Steve Ballmer did not know his customers (specifically, his customers who were developers). For a long time, Microsoft did not seem to resonate with developers because they made quite a few flaws: lack of open source involvement, slow or laggard IE development cycles, and tooling lock-in, for starters.

Once Microsoft ousted Ballmer in favor of Nadella, this all changed. Microsoft [joined the Open Source Initiative](https://opensource.org/node/901). They also joined [The Linux Foundation](https://www.zdnet.com/article/microsoft-completes-its-linux-journey-joins-the-linux-foundation/). And of all companies, GitHub admitted that Microsoft was the [#1 contributor to open source on that platform in 2017](https://octoverse.github.com/).

And this isn't even about making peace, this is about money. And money talks:

![Microsoft is better with open source](/assets/images/msft-better-w-open-source.png)

Note that the cursor in the screenshot is pointed right when Nadella took over. When they corrected their ship, this made a _huge_ impact on their bottom line. In fact, it's worked so well that they are now once again [bigger than Alphabet](https://www.cnbc.com/2018/05/29/microsoft-passes-alphabet-by-market-cap.html). What is the bottom line?

**Microsoft is financially incentivized to not screw this up.**

They've already invested so much into open source. Let's say, worst case scenario, they were to turn their back on OSS and decide to introduce some truly draconian terms for GitHub. History has shown those kinds of moves completely stall their stock. This is what Ballmer did and the company suffered financially. They don't want to repeat the same mistake twice.

## Don't forget LinkedIn

Microsoft has been involved with a social community before. [Nearly two years ago to the day they acquired LinkedIn for nearly x4 more](https://news.microsoft.com/2016/06/13/microsoft-to-acquire-linkedin/). You know what happened?

Not all that much.

In fact, the change in terms of service was so inconsequential that [LinkedIn noted](https://blog.linkedin.com/2017/april/18/updates-to-our-terms-of-service) that they would not be influenced by Microsoft under blanket terms of service:

> You might have heard that at the end of last year we became a part of Microsoft, but our services are still provided under a separate Terms of Service. We’re excited about leveraging Microsoft technology and resources to offer more valuable features and services to you.

And I've been on LinkedIn for the last 2 years. I'm neither frustrated nor thrilled. LinkedIn is just as much of a recruiter Zombieland as it has always been. You don't have to trust Microsoft when they say they don't want to change GitHub, but they are not offering any indication that they want to mess with it.

# Myth: GitLab and Bitbucket are safer options

The other issue I see is that if there is an unwavering hatred for Microsoft, that we must all leave for better pastures. The two biggest players in the space that are still independent are GitLab and Bitbucket. Are they _really_ all that much better than GitHub?

## They're all for-profit businesses

The most remarkable thing to me in reading the public's reaction is that people make it seem like GitHub was an open source non-profit company. That because they were the premiere company to publish your open source code, it must mean they were pure in intentions.

They're very much a for-profit and primarily make money off of storing private repositories. GitLab and Atlassian are no different! In fact, now that GitHub is part of Microsoft, they're a public company just like Atlassian.

Even though the business models for each are somewhat different in how they make money off of the code storage, the fact is each of those companies is closed source, with specific legal terms to designate what is owned by the contributors, and what is owned by the storage company. And they're _all_ trying to make money (and there's nothing wrong with that).

## You can still deploy your code to mega-corps

Even if you go with GitLab or Bitbucket, where are you going to deploy your code? Because unless you plan on buying your own servers and scaling like we did in the 90s and 2000s, you're going to use one of a few big players:

1. DigitalOcean. A company whose debt is mostly [financed by megabanks](https://www.crunchbase.com/organization/digitalocean#section-locked-charts) like HSBC and Barclays.
2. Heroku. A company now owned by Salesforce, a publicly traded company worth nearly $100BB.
3. Amazon Web Services. A company with a bigger market cap than Microsoft.
4. Azure. Which is owned by you know who...

So even if you self-host your code for the sake of using git with your team, you're still handing over your code to another massive company when you go to turn the lights on for customers. And you may have already done that deed with Microsoft.

## Look at the terms

The GitHub terms of service only mention open source [in section D.6](https://help.github.com/articles/github-terms-of-service/#d-user-generated-content):

>  This is widely accepted as the norm in the open-source community; it's commonly referred to by the shorthand "inbound=outbound". We're just making it explicit.

This isn't even in the ownership section, which clearly states:

> You retain ownership of and responsibility for Your Content...we need you to grant us — and other GitHub Users — certain legal permissions, listed in Sections D.4 — D.7. These license grants apply to Your Content. If you upload Content that already comes with a license granting GitHub the permissions we need to run our Service, no additional license is required. You understand that you will not receive any payment for any of the rights granted in Sections D.4 — D.7. The licenses you grant to us will end when you remove Your Content from our servers unless other Users have forked it.

You can continue to read the rest of those sections, but the real key nugget that you're probably worried about is the legal permissions that apply to you:

> We need the legal right to do things like host Your Content, publish it, and share it. You grant us and our legal successors the right to store, parse, and display Your Content, and make incidental copies as necessary to render the Website and provide the Service. This includes the right to do things like copy it to our database and make backups; show it to you and other users; parse it into a search index or otherwise analyze it on our servers; share it with other users; and perform it, in case Your Content is something like music or video.
>
> **This license does not grant GitHub the right to sell Your Content or otherwise distribute or use it outside of our provision of the Service.**

The rest of those sections simply state that if you post public code it can be viewed by anyone, if you contribute to a repo it's bound to the repo's declared license, and when you upload code it comes with "moral rights," meaning you have the right to seek attribution and credit for the code you have published.

So how does this compare to ownership and IP for GitLab and Atlassian? [For GitLab](https://about.gitlab.com/terms/#subscription):

> 4.3 Customer and its licensors shall (and Customer hereby represents and warrants that they do) have and retain all right, title and interest (including, without limitation, sole ownership of) all software, information, content and data provided by or on behalf of Customer or made available or otherwise distributed through use of the Licensed Materials (“Content”) and the intellectual property rights with respect to that Content.

And [for Atlassian](https://www.atlassian.com/legal/customer-agreement):

> 7.4 Your Data. “Your Data” means any data, content, code, video, images or other materials of any type that you upload, submit or otherwise transmit to or through Hosted Services. You will retain all right, title and interest in and to Your Data in the form provided to Atlassian. Subject to the terms of this Agreement, you hereby grant to Atlassian a non-exclusive, worldwide, royalty-free right to (a) collect, use, copy, store, transmit, modify and create derivative works of Your Data, in each case solely to the extent necessary to provide the applicable Hosted Service to you and (b) for Hosted Services that enable you to share Your Data or interact with other people, to distribute and publicly perform and display Your Data as you (or your Authorized Users) direct or enable through the Hosted Service. Atlassian may also access your account or instance in order to respond to your support requests.

**TL;DR: you own your code.** In fact, if there was one company in that three that I am worried about, it's Atlassian. **Atlassian can collect, copy, store, modify, and use your data.** This isn't spelled out with GitLab or GitHub. So don't think the alternatives are any better for you now that Microsoft owns GitHub.

# Assuming it's all good, what can go wrong?

So if you're convinced that you don't need to jump ship, the truth is things _could_ go wrong. Here are the things to look for when the deal ultimately goes through and the acquisition is 100% complete:

1. **Look out for a GitHub conversion announcement.** Just because they've announced an acquisition does not mean the services have completely rolled over to Microsoft. In fact, the current terms of service for GitHub are dated May 25th which is pre-announcement. If things change and either GitHub or Microsoft announces the rollover is complete, make sure to read the changes carefully. If they change their terms, they'll make you accept or reject the new terms.
2. **If they introduce new terms, see who owns them.** As I mentioned earlier in LinkedIn's conversion announcement, they stated that they still owned their own separate terms of service. This will likely be the case for GitHub as well. If that isn't the case, you'll have to read Microsoft's terms as well as GitHub's. Like class inheritance, whatever you read on GitHub is layered on top of what Microsoft is applying to their software licenses.
3. **If the terms change, specifically check Sections D.3-D.7 for change in ownership or rights.** That's the core section that deals with who owns and distributes the code. If this becomes more restrictive or unfavorable to your community, that would be a good time to re-evaluate your source code hosting strategy.

Is this acquisition a big deal? Yes. Is it a bad deal for you? Probably not. Keep calm and code on, but if the terms change, read them carefully and re-evaluate if they are no longer favorable to your interests.
