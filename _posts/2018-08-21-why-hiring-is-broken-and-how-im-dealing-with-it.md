---
title: Why hiring is broken and how I'm dealing with it
description: I think I've figured out why hiring is so broken in 2018.
layout: post
date: '2018-08-28 10:28:00'
tags: hiring interviewing data-structures algorithms
subclass: post tag-test tag-content
categories: computer-science
navigation: 'True'
image: 'ds-algo.png'
article: 'True'
---

Programming interviews are a major source of anxiety and angst among the software community. Every few months you see a post on HackerNews saying that we need to seriously rethink how we should be interviewing candidates in the software development profession. The primary reason has to do with how programming interviews are structured today.

## The typical interview process in 2018

If you've ever interviewed at one of the big FAANG (Facebook Amazon Apple Netflix Google) companies you've noticed a trend in how candidates are interviewed, regardless of what kind of position you're interviewing for. The format looks something like this:

* **A recruiter reaches out asking if you want to interview with [FAANG_COMPANY]**
* **30-45 min phone screener**
* **4-5 in-person interviews with a combination of fellow software engineers, engineering managers, and HR**

The real nugget of frustration lies in the type of interview questions they ask. Whether you're an infrastructure engineer or a front-end developer, **these companies love to ask data structure and algorithm questions on a whiteboard**. If you aren't prepared, you can get _rocked_ by them. There are two approaches people seem to take in response:

1. **Interview where this doesn't happen.** Don't want to be subjected to arbitrary interview questions from your CS classes with things you never do anymore? Don't play the game!
2. **Embrace the flawed process and game the system.** In many ways, today's interviews are like studying for the SAT. They aren't an accurate reflection of what you'll actually be doing, but they do have a predictable format that you can study to maximize your chances of success.

But is there a third, better way?

## Why hiring is the way it is today

For a while, I was in the first camp and believed that these tests were contrived, led to too many false negatives (missing good candidates who simply didn't excel in these superficial conditions), and didn't actually measure programming aptitude under real conditions. I've conducted probably over 100 interviews in my career and I will admit I have done both non-whiteboard and whiteboard interview questions with mixed results in both.

But then I realized something.

To preface my hypothesis, I will state right away that this isn't backed up by anything factual since I have never worked at either Facebook or Google. However, given these are highly-desirable companies to work for, I have now come around to understanding why I think hiring is in the tumultuous state it is in:

**Google and Facebook's core businesses revolve around algorithmic problems of large, connected sparse graphs. Therefore, they ask these questions to judge if their candidates can operate on all programming aspects of their business. As a consequence of their success, other companies have followed suit even if their core business is not solving a graph problem.**

Whether it is a network of web pages or a network of people's digital profiles, these businesses, at their core, are about efficiently managing _billions_ of nodes in a connected graph. Problems at this kind of scale cannot be managed without at least some knowledge of graph data structures and algorithms for efficiently interacting with each other.

When you look at it this way, it starts to make sense as to why a company like Google would prefer to ask questions like this. The problem is that other companies have seen Google and Facebook's breakout success and have decided to emulate them without carefully understanding their own business and what is required of their engineers to perform well.

One analogy that resonates with me is when young teenagers read a bodybuilding magazine and decide that if they want to look like Arnold Schwarzenegger, they have to train like him. So the publication outlines Arnold's winning workout that leads him to victory at Mr. Olympia.

Do you see the flaw there? They're showcasing advanced athletes with their advanced workouts. Instead, what you really want to see is **what the successful athletes did as beginners to get to the next stage on their path to ultimate success**.

Put it another way: you don't want to emulate Airbnb's or Stripe's homepage today if you want to look at how to market a successful startup. Instead, you want to look at Airbnb's homepage in 2008, or Stripe's in 2010.

**It is far better to understand success through the _whole journey_, and not just at the end when success has already been attained.**

Getting back to hiring, I think businesses suffer from this same issue, and emulate the FAANG with its current success, rather than look at their earlier hiring practices, or at least take a step back and critically evaluate _why_ they chose to hire the way the did. Perhaps with that insight, we might have businesses who hire to truly reflect the capabilities required of the jobs they are paying for.

## So then why are you doing this?

"Okay", you might say, "so even with all of that said, why are you still in favor of learning data structures and algorithms after being in the business for 10 years?"

Good question! It comes down to four primary reasons:

### If you can do this, you can pretty much handle any interview

As I mentioned earlier, the second camp of folks has embraced that this is our reality in hiring right now. There are [lots](https://www.amazon.com/Elements-Programming-Interviews-Python-Insiders/dp/1537713949/ref=sr_1_1?ie=UTF8&qid=1526172412&sr=8-1&keywords=elements+of+programming+interviews+python) of great [books](https://www.amazon.com/Cracking-Coding-Interview-Programming-Questions/dp/0984782850) that give you the background plus targeted practice to essentially guarantee entry into the coveted FAANG community.

I personally know 4 people in the last year who used one of those two books and put in roughly 80-100 hours of study and landed jobs at Google, Facebook, and Amazon.

This stuff works, plain and simple. Put another way: if you knew that with just 100 hours of work, 2.5 weeks of full-time study, you could _drastically_ increase your chances of earning well into the six figures of income, would you put in the time?

Even if you know that you're "above these games," or that "your work speaks for itself," is it really _that_ much to ask to take what amounts to one semester of college coursework to **have the peace of mind that whether it's Google or some small startup, you'll always pass a tech interview and get a great job?**

Once I looked at it like this, it was hard _not to_ give this a shot, especially considering I never took a dedicated algorithms class in college.

### I enjoy teaching people

One of my favorite jobs of the past few years was acting as an engineering manager. I was fortunate to still spend quite a bit of my time coding, but I was also able to mentor junior engineers. I get as much back as I give when I'm teaching someone something for the first time.

I believe that anyone can do this thing called programming. My hope is that, given the right approach and materials, this can be taught in a practical way that isn't subject to the academic rigidity of a textbook or classroom. To be fair, I will be using a textbook and college course to guide my notes, but my hope is that my interpretation will clear up some of the murky confusion that often arises from overly dense courses.

**If you want a new way of learning the fundamentals and nothing else has worked, maybe this will.**

### Teaching reinforces learning

If you've heard of the renowned [learning how to learn course](https://pt.coursera.org/learn/learning-how-to-learn) you know that the best way to learn something is to teach it to someone else. So yes, as selfish motivation for me to permanently learn this material, I will be employing many of that course's techniques (e.g. chunking, spaced repetition, teaching, alternative notetaking methods) to solidify my understanding of this topic.

### This is part of something even bigger

Lastly, one of my prime motivations for doing all of this is because **I think this is necessary background before one can start to appreciate the fundamentals of front-end UI development.**

Eventually, I'd like to extend this to a second series on the fundamentals of UI frameworks like Angular and React. Both of these frameworks are deeply rooted in understanding how the DOM works.

But before you can truly understand how the DOM works, you need to understand how a tree works. And to understand how a tree works, it helps to understand how data structures like lists and arrays work. When I kept peeling back the layers, I realized it just made more sense to start with the fundamentals of computer science and programming.

## How this is going to work

It is nearing the end of summer and college classes are starting up again. It turns out one of those courses is [Steven Skiena's CSE 373 Analysis of Algorithms class](http://www3.cs.stonybrook.edu/~skiena/373/). Skiena is famous for his [Algorithm Design Manual](http://www.algorist.com/), a book considered by many to be the most approachable introduction to data structures and algorithms. Even Steve Yegge, the famous blogger and Xoogler, [recommends the book](https://steve-yegge.blogspot.com/2008/03/get-that-job-at-google.html) above other canonical texts like _Cracking the Coding Interview_ or the mega textbook, _Algorithms_.

Given I've tried my hand at _The Elements of Programming Interviews_, _Cracking the Coding Interview_, _Algorithms_, and _The Competitive Programmer's Handbook_, all without success, I thought I'd give this one a try. In addition to owning a copy of the book, I figured the combination of the book plus the actual live lectures and course materials might help fill in the gaps from just studying a textbook on your own.

In other words, **I'm auditing Skiena's algorithms course and this series will be my notes as I attempt to learn data structures and algorithms once and for all.** And hopefully, I'll help others along the way.

### Structure

As this is a college course, I imagine many of the exercises and sections are simply not applicable to why you might follow along with this series: to ace a technical interview. So if I see something in the course that you'll likely never see in an interview, I'll acknowledge it was covered, but I won't say too much so we only need to learn exactly what will be useful to us.

#### Each post will follow each lecture

Because I'm going to follow along with CSE 373, I'm going to be taking notes as if I were a student in the course. Those notes will then be transcribed and presented in a teachable format to both reinforce my learning of the material but also to provide a useful filter for my fellow professionals.

#### Each post will cover the publically-available exercises

In case this somehow gets picked up enough to warrant the attention of actual students taking the class, **no material will be published before it becomes publicly available.** This means that if there are homework problems assigned, I will not be publishing my attempts at a solution until the course has published solutions (if ever).

As much as I'd truly love to give you a deep dive into every insight of someone taking the course, I don't think it's fair to the instructor or the students to turn this series into a cheater's reference guide to getting through the course (and hey, I might be wrong on a few things).

#### Each post will provide additional commentary where appropriate

Finally, my hope is to add insight into questions I might have or relevant connections to other material that might help bolster everyone's learning of the material. I certainly won't have all of the answers as I take this course, but my hope is this can be a launching point into more discussion [on Twitter](https://twitter.com/theadamconrad) or other forms of social media so we can figure this all out together.
