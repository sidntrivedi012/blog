---
title: Get updates straight to your mailbox
layout: page
class: post
navigation: 'True'
current: newsletter
---

We provide a weekly newsletter for both email and RSS featuring a highly curated list of articles in the field of user interface development. These are hand-crafted each and every week by [Adam](/about/) to ensure the highest level of relevance and quality.
## Why sign up for the list
The two biggest reasons to sign up for the newsletter are **content quality** and **exclusive articles** you won't find anywhere else. We try to offer a few advantages in our newsletter that we haven't seen anywhere else:
### Brief, but up-to-date
We aren't sure why, but we always thought the whole point of newsletters was so that you didn't have to sift through a ton of stuff. And yet, most email newsletters are still super dense. **We try to keep the number of articles to less than 10 so you only see the best content**. We also make sure they are relevant to what you're interested in now, not last year. You won't likely ever find a throwback article, particularly in a field that is changing so rapidly.
### Organized by experience level
It's really hard to judge from a title (outside of the few extremely obvious ones) what articles are aimed at beginners versus seasoned veterans. **Wouldn't it be great if you could skip all of the articles that are too easy (or too advanced)?** We thought you'd say yes. We also make sure we provide a few extras, as well as articles on the bleeding-edge, so there's always something new to discover, regardless of your experience with UI.
### You'll learn something new
The software disciplines attract people who naturally crave knowledge. **You deserve a newsletter that actually helps you move forward in your career.** We tend to skip purely opinion pieces and fluff articles, in favor of articles that teach and instruct. That doesn't mean they're necessarily technical; lots of great articles can teach about soft skills and aspects outside of design and programming, and we won't shy away from those. We will also make sure that if it is an opinion piece, it provides at least one takeaway.

Not sure about an article? **We annotate every link with a summary.** So even if you can only read the headline, we'll follow up with either why you should read it, or what you should take away from the article.

## What are you waiting for?

Sign up now and start getting all of our content (and _way_ more) delivered right to your inbox!

<div id="mc_embed_signup"><form action="https://userinterfacing.us16.list-manage.com/subscribe/post?u=3ceb8ec80146954c9878cc93c&amp;id=7841e9b74e" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate=""><div id="mc_embed_signup_scroll"><input type="email" value="" name="EMAIL" class="email" id="mce-EMAIL" placeholder="my.email@example.com" required=""><div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_3ceb8ec80146954c9878cc93c_7841e9b74e" tabindex="-1" value=""></div><div class="clear"><input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button"></div></div></form></div>

Prefer RSS? [Subscribe here](/rss.xml/).
