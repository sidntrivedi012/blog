---
layout: page
title: About
class: 'post'
navigation: True
current: about
---

This is the blog of [Adam C. Conrad](https://www.conradadam.com), it contains thoughts on helping startups move faster through technical scalability, team scalability, engineering best practices, and technical performance optimization.

The blog and all of it's code is completely open source and is hosted by GitLab with Jekyll on GitLab Pages. [Fork the blog](https://gitlab.com/userinterfacing/blog)!

We offer training and consulting for companies and individuals looking to take organizations to the next level. Head on over to [Anon Consulting](https://www.anonconsulting.com) and send us an email and we'd be happy to get you moving faster and delivering more value to your customers!
